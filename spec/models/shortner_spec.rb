require "spec_helper"
require './models/shortner'

RSpec.describe Shortner, type: :model do
  
  describe "Shortner.gernerate_code" do
    it "should generate a code of 6 characters" do
      expect(Shortner.gernerate_code).to  match(Shortner::SHORT_CODE_REGEX)
    end
  end

  describe "validations" do
    it { should validate_presence_of(:url) }
    it { should validate_presence_of(:shortcode) }
    it { should validate_numericality_of(:redirect_count)}
    it "should validate uniqeness of shortcode" do
      shortcode = Shortner.gernerate_code
      shortner = Shortner.new(url: "https://wwww.google.com", shortcode: shortcode)
      expect(shortner.valid?).to be_truthy
      shortner.save
      shortner2 = Shortner.new(url: "https://wwww.google.com", shortcode: shortcode)
      expect(shortner2.valid?).to be_falsey
      expect(shortner2.errors.messages.keys).to include(:shortcode)
      expect(shortner2.errors.messages[:shortcode]).to include(Shortner::SHORTCODE_UNIQUE_MESSAGE)
    end
    it "should validate url is not blank" do
      shortcode = Shortner.gernerate_code
      shortner = Shortner.new(url: "", shortcode: shortcode)
      expect(shortner.valid?).to be_falsey
      expect(shortner.errors.messages.keys).to include(:url)
      expect(shortner.errors.messages[:url]).to include(Shortner::URL_BLANK_MESSAGE)
      shortner.url = "https://wwww.google.com"
      expect(shortner.valid?).to be_truthy
    end
    it "should validate shortcode is not blank" do
      shortcode = Shortner.gernerate_code
      shortner = Shortner.new(url: "https://wwww.google.com", shortcode: "")
      expect(shortner.valid?).to be_falsey
      expect(shortner.errors.messages.keys).to include(:shortcode)
      expect(shortner.errors.messages[:shortcode]).to include(Shortner::SHORTCODE_BLANK_MESSAGE)
      shortner.shortcode = shortcode
      expect(shortner.valid?).to be_truthy
    end
    it "should validate shortcode match regex" do
      shortcode = Shortner.gernerate_code
      shortner = Shortner.new(url: "https://wwww.google.com", shortcode: "")
      expect(shortner.valid?).to be_falsey
      expect(shortner.errors.messages.keys).to include(:shortcode)
      expect(shortner.errors.messages[:shortcode]).to include(Shortner::SHORTCODE_FORMAT_MESSAGE)
      shortner.shortcode = shortcode
      expect(shortner.valid?).to be_truthy
    end
    it "should validate redirect_count is a positive number" do
      shortcode = Shortner.gernerate_code
      shortner = Shortner.new(url: "https://wwww.google.com", shortcode: shortcode)
      shortner.redirect_count = -1
      expect(shortner.valid?).to be_falsey
      expect(shortner.errors.messages.keys).to include(:redirect_count)
      expect(shortner.errors.messages[:redirect_count]).to include(Shortner::REDIRECT_COUNT_ERROR_MESSAGE)
      shortner.redirect_count = 0
      expect(shortner.valid?).to be_truthy
      shortner.redirect_count = 12
      expect(shortner.valid?).to be_truthy
    end
  end

  context "WHEN increment is called" do
    it "should change the updated_at field" do
      shortcode = Shortner.gernerate_code
      shortner = Shortner.new(url: "https://wwww.google.com", shortcode: shortcode)
      last_date = 2.days.ago
      shortner.created_at = last_date
      shortner.updated_at = last_date
      expect(shortner.valid?).to be_truthy
      shortner.save
      shortner.increment(redirectCount: 1)
      expect(shortner.updated_at.iso8601).not_to be == last_date.iso8601
    end
  end  
  
end
