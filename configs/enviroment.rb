require 'rubygems'
require 'bundler'
require 'dotenv/load'
#require 'mongo_mapper'
@env = ENV["SHORTLY_ENV"] || "development"
Bundler.require(:default, @env)

